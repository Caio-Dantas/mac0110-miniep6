# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    #Pelo esquema de Nicômaco
    return n^2 - n + 1
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    #Calcula o primeiro ímpar da sequência
    primeiro_impar = m^2 - m + 1
    #Exibe o valor recebido e o cubo do mesmo
    print(m, " ", m^3)
    #Loop para exibir a decomposição do m ao cubo em parcelas
    for i in 1:m
        print(" ", primeiro_impar)
        primeiro_impar += 2
    end
end

function mostra_n(n)
    #Laço para exibir a decomposição de 1 até n.
    for i in 1:n
        imprime_impares_consecutivos(i)
        println("")
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
 test()
